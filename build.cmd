@echo off
cmd /c "cd /d Trader && nuget restore"
cmd /c "cd /d Trader && msbuild /m /nr:false /t:Clean,Rebuild /p:Configuration=Release /p:Platform="Any CPU" /verbosity:minimal /p:CreateHardLinksForCopyLocalIfPossible=true"

echo.