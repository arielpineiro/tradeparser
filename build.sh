#!/bin/bash
( cd Trader ; nuget restore )
( cd Trader ; msbuild /m /nr:false /t:Clean,Rebuild /p:Configuration=Release /p:Platform="Any CPU" /verbosity:minimal /p:CreateHardLinksForCopyLocalIfPossible=true )

echo