using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;

namespace Trader.CommandLineParser.Migrations
{
    /// <summary>
    ///     Migration
    /// </summary>
    /// <seealso>
    ///     <cref>System.Data.Entity.Migrations.DbMigration</cref>
    /// </seealso>
    /// <seealso cref="IMigrationMetadata" />
    public partial class CreateDatabase : DbMigration
    {
        /// <summary>
        ///     Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            CreateTable(
                    "dbo.Options",
                    c => new
                    {
                        Id = c.Int(false, true),
                        OptionChain_Id = c.Int(false),
                        Last = c.Double(false),
                        Mark = c.Double(false),
                        Bid = c.Double(false),
                        Ask = c.Double(false),
                        Strike = c.Double(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OptionChains", t => t.OptionChain_Id, true)
                .Index(t => t.OptionChain_Id);

            CreateTable(
                    "dbo.OptionChains",
                    c => new
                    {
                        Id = c.Int(false, true),
                        StockAndOptionQuote_Id = c.Int(false),
                        Date = c.DateTime(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StockAndOptionQuotes", t => t.StockAndOptionQuote_Id, true)
                .Index(t => t.StockAndOptionQuote_Id);

            CreateTable(
                    "dbo.StockAndOptionQuotes",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Date = c.DateTime(false),
                        ThinkBack = c.String(maxLength: 255),
                        Symbol_Id = c.Int(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Symbols", t => t.Symbol_Id, true)
                .Index(t => t.Symbol_Id);

            CreateTable(
                    "dbo.Symbols",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Name = c.String(false, 50)
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);

            CreateTable(
                    "dbo.Underlyings",
                    c => new
                    {
                        Id = c.Int(false),
                        Last = c.Double(false),
                        NetChng = c.Double(false),
                        Open = c.Double(false),
                        High = c.Double(false),
                        Low = c.Double(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StockAndOptionQuotes", t => t.Id)
                .Index(t => t.Id);

            CreateTable(
                    "dbo.Calls",
                    c => new
                    {
                        Id = c.Int(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Options", t => t.Id)
                .Index(t => t.Id);

            CreateTable(
                    "dbo.Puts",
                    c => new
                    {
                        Id = c.Int(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Options", t => t.Id)
                .Index(t => t.Id);
        }

        /// <summary>
        ///     Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            DropForeignKey("dbo.Puts", "Id", "dbo.Options");
            DropForeignKey("dbo.Calls", "Id", "dbo.Options");
            DropForeignKey("dbo.Options", "OptionChain_Id", "dbo.OptionChains");
            DropForeignKey("dbo.OptionChains", "StockAndOptionQuote_Id", "dbo.StockAndOptionQuotes");
            DropForeignKey("dbo.Underlyings", "Id", "dbo.StockAndOptionQuotes");
            DropForeignKey("dbo.StockAndOptionQuotes", "Symbol_Id", "dbo.Symbols");
            DropIndex("dbo.Puts", new[] { "Id" });
            DropIndex("dbo.Calls", new[] { "Id" });
            DropIndex("dbo.Underlyings", new[] { "Id" });
            DropIndex("dbo.Symbols", new[] { "Name" });
            DropIndex("dbo.StockAndOptionQuotes", new[] { "Symbol_Id" });
            DropIndex("dbo.OptionChains", new[] { "StockAndOptionQuote_Id" });
            DropIndex("dbo.Options", new[] { "OptionChain_Id" });
            DropTable("dbo.Puts");
            DropTable("dbo.Calls");
            DropTable("dbo.Underlyings");
            DropTable("dbo.Symbols");
            DropTable("dbo.StockAndOptionQuotes");
            DropTable("dbo.OptionChains");
            DropTable("dbo.Options");
        }
    }
}