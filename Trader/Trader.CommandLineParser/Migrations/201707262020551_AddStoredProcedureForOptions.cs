using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;

namespace Trader.CommandLineParser.Migrations
{
    /// <summary>
    ///     Migration
    /// </summary>
    /// <seealso>
    ///     <cref>System.Data.Entity.Migrations.DbMigration</cref>
    /// </seealso>
    /// <seealso cref="IMigrationMetadata" />
    public partial class AddStoredProcedureForOptions : DbMigration
    {
        /// <summary>
        ///     The install script
        /// </summary>
        private const string InstallScript =
            @"USE Trader;

            GO
            CREATE PROCEDURE GetOptionsByDateAndSymbol
                @Date Date,
            	@Symbol nvarchar(50)
            AS
                SELECT
            	symbols.Name,
            	stockAndOptionsQuotes.Date AS 'File Date',
            	options.Last,
            	options.Mark,
            	options.Bid,
            	options.Ask,
            	options.Strike,
            	optionChains.Date AS 'Option Chain Date'
            FROM Trader.dbo.OptionChains optionChains
            	JOIN dbo.Options options ON options.OptionChain_Id = optionChains.Id
            	LEFT JOIN dbo.Calls calls ON calls.Id = options.Id
            	LEFT JOIN dbo.Puts puts ON puts.Id = options.Id
            	JOIN dbo.StockAndOptionQuotes stockAndOptionsQuotes ON stockAndOptionsQuotes.Id = optionChains.StockAndOptionQuote_Id
            	JOIN dbo.Symbols symbols ON symbols.Id = stockAndOptionsQuotes.Symbol_Id
            WHERE optionChains.Date = @Date AND symbols.Name = @Symbol
            GO  ";

        /// <summary>
        ///     The uninstall script
        /// </summary>
        private const string UninstallScript =
            @"USE [Trader]

            GO
            DROP PROCEDURE [dbo].[GetOptionsByDateAndSymbol]
            GO";

        /// <summary>
        ///     Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            Sql(InstallScript);
        }

        /// <summary>
        ///     Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            Sql(UninstallScript);
        }
    }
}