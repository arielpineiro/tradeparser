using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;

namespace Trader.CommandLineParser.Migrations
{
    /// <summary>
    ///     Migration
    /// </summary>
    /// <seealso>
    ///     <cref>System.Data.Entity.Migrations.DbMigration</cref>
    /// </seealso>
    /// <seealso cref="IMigrationMetadata" />
    public partial class AddUniqueIndex : DbMigration
    {
        /// <summary>
        ///     Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            CreateIndex("dbo.StockAndOptionQuotes", new[] { "Date", "Symbol_Id" }, true);
        }

        /// <summary>
        ///     Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            DropIndex("dbo.StockAndOptionQuotes", new[] { "Date", "Symbol_Id" });
        }
    }
}