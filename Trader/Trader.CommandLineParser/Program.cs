﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Trader.CommandLineParser.Model;
using static System.Configuration.ConfigurationManager;
using static System.Console;
using static System.Convert;
using static System.IO.Directory;
using static System.IO.File;
using static System.IO.Path;
using static System.StringComparison;

namespace Trader.CommandLineParser
{
    /// <summary>
    ///     Main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        ///     The folder
        /// </summary>
        private static readonly string Folder = AppSettings["Folder"].Replace('\\', DirectorySeparatorChar);

        /// <summary>
        ///     The result
        /// </summary>
        private static readonly Result Result = new Result();

        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        private static void Main()
        {
            var files = EnumerateFiles(Folder, "*.csv").ToList();

            WriteLine();
            WriteLine($@" Folder: {Folder}");
            WriteLine();

            foreach (var file in files)
                try
                {
                    var stockAndOptionQuote = Process(file);
                    InsertToDataBaseAsync(stockAndOptionQuote).Wait();
                }
                catch (DbUpdateException e)
                {
                    Result.Failed++;
                    WriteLine(e.InnerException?.InnerException?.Message);
                }
                catch (Exception e)
                {
                    Result.Failed++;
                    WriteLine(e);
                }

            WriteLine();
            WriteLine($@" {files.Count} file(s) was processed");
            WriteLine($@"   Saved: {Result.Saved}");
            WriteLine($@"   Ignored: {Result.Ignored}");
            WriteLine($@"   Failed: {Result.Failed}");
        }

        /// <summary>
        ///     Processes the specified file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        private static StockAndOptionQuote Process(string file)
        {
            Write($@"	Processing {GetFileName(file)} ... ");

            // Parsing file
            var stockAndOptionQuote = ParseFile(file);

            return stockAndOptionQuote;
        }

        /// <summary>
        ///     Inserts to data base asynchronous.
        /// </summary>
        /// <param name="stockAndOptionQuote">The stock and option quote.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        private static async Task InsertToDataBaseAsync(StockAndOptionQuote stockAndOptionQuote)
        {
            using (var db = new TraderDb())
            {
                var name = stockAndOptionQuote.Symbol.Name;

                var symbol = await db.Symbols.FirstOrDefaultAsync(x => x.Name.Equals(name));

                if (symbol == null)
                {
                    symbol = new Symbol { Name = name };
                    db.Symbols.Add(symbol);
                    await db.SaveChangesAsync();
                    Write(@"New symbol was added! ");
                }

                stockAndOptionQuote.Symbol = symbol;

                var result = await
                    (from item in db.StockAndOptionQuotes
                     where item.Date == stockAndOptionQuote.Date &&
                           item.Symbol.Name.Equals(name)
                     select item)
                    .FirstOrDefaultAsync();

                if (result == null)
                {
                    db.StockAndOptionQuotes.Add(stockAndOptionQuote);
                    await db.SaveChangesAsync();
                    WriteLine(@"Saved. ");
                    Result.Saved++;
                }
                else
                {
                    WriteLine(@"Skipped, already exists. ");
                    Result.Ignored++;
                }
            }
        }

        /// <summary>
        ///     Parses the file.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        private static StockAndOptionQuote ParseFile(string file)
        {
            var stockAndOptionQuote = new StockAndOptionQuote
            {
                Symbol = new Symbol
                {
                    Name = GetSymbol(file)
                },
                Date = GetDate(file)
            };
            using (TextReader textReader = OpenText(file))
            {
                var csvReader = new CsvReader(textReader, CultureInfo.InvariantCulture)
                {
                    Configuration =
                    {
                        HasHeaderRecord = true,
                        Delimiter = ","
                    }
                };

                GetHeader(csvReader, stockAndOptionQuote);
                GetUnderlying(csvReader, stockAndOptionQuote);

                stockAndOptionQuote.OptionsChains = new List<OptionChain>();

                while (csvReader.Read())
                {
                    var optionChain = new OptionChain
                    {
                        Options = new List<Option>()
                    };

                    if (IsOptionHeaderDate(csvReader))
                    {
                        // Date Header Call & Put
                        optionChain.Date = ToDateTime(csvReader.GetField<string>(0).Substring(0, 9));
                        stockAndOptionQuote.OptionsChains.Add(optionChain);
                    }
                    else
                    {
                        if (!IsOptionHeader(csvReader))
                            AddRecord(stockAndOptionQuote, csvReader);
                    }
                }
            }

            return stockAndOptionQuote;
        }

        /// <summary>
        ///     Gets the date.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        private static DateTime GetDate(string file)
        {
            var date = GetFileName(file)?.Substring(0, 10);
            return ToDateTime(date);
        }

        /// <summary>
        ///     Gets the symbol.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        private static string GetSymbol(string file)
        {
            var indexOf = file.IndexOf("For", Ordinal);
            var name = file.Substring(indexOf + 3).Split('.')[0];
            return name;
        }

        /// <summary>
        ///     Adds the record.
        /// </summary>
        /// <param name="stockAndOptionQuote">The stock and option quote.</param>
        /// <param name="csvReader">The CSV reader.</param>
        private static void AddRecord(StockAndOptionQuote stockAndOptionQuote, CsvReader csvReader)
        {
            var expirationField = csvReader.GetField<string>(6);
            BaseBuilder builder = new Builder(csvReader);
            var call = builder.CreateCall();
            var put = builder.CreatePut();
            SearchAndAdd(stockAndOptionQuote, expirationField, call, put);
        }

        /// <summary>
        ///     Searches the and add.
        /// </summary>
        /// <param name="stockAndOptionQuote">The stock and option quote.</param>
        /// <param name="expirationField">The expiration field.</param>
        /// <param name="call">The call.</param>
        /// <param name="put">The put.</param>
        private static void SearchAndAdd(StockAndOptionQuote stockAndOptionQuote, string expirationField, Option call,
            Option put)
        {
            var key = ToDateTime(expirationField);
            var chain = stockAndOptionQuote.OptionsChains.First(x => x.Date.Equals(key));
            chain.Options.Add(call);
            chain.Options.Add(put);
        }

        /// <summary>
        ///     Determines whether [is option header] [the specified CSV reader].
        /// </summary>
        /// <param name="csvReader">The CSV reader.</param>
        /// <returns>
        ///     <c>true</c> if [is option header] [the specified CSV reader]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsOptionHeader(CsvReader csvReader)
        {
            return csvReader.Context.Record.FirstOrDefault(record => record.Contains("Strike")) != null;
        }

        /// <summary>
        ///     Determines whether [is option header date] [the specified CSV reader].
        /// </summary>
        /// <param name="csvReader">The CSV reader.</param>
        /// <returns>
        ///     <c>true</c> if [is option header date] [the specified CSV reader]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsOptionHeaderDate(CsvReader csvReader)
        {
            return csvReader.Context.Record.Length == 1;
        }

        /// <summary>
        ///     Gets the header.
        /// </summary>
        /// <param name="csvReader">The CSV reader.</param>
        /// <param name="stockAndOptionQuote">The stock and option quote.</param>
        private static void GetHeader(CsvReader csvReader, StockAndOptionQuote stockAndOptionQuote)
        {
            csvReader.Read();
            stockAndOptionQuote.ThinkBack = (csvReader.Context as ReadingContext)?.RawRecord;
        }

        /// <summary>
        ///     Gets the underlying.
        /// </summary>
        /// <param name="csvReader">The CSV reader.</param>
        /// <param name="stockAndOptionQuote">The stock and option quote.</param>
        private static void GetUnderlying(CsvReader csvReader, StockAndOptionQuote stockAndOptionQuote)
        {
            while (csvReader.Read() && csvReader.Context.Row < 5)
            {
                // Skip new lines for Main header
            }

            stockAndOptionQuote.Underlying = new Underlying
            {
                Last = csvReader.GetField<double>(0),
                NetChng = csvReader.GetField<double>(1),
                Open = csvReader.GetField<double>(3),
                High = csvReader.GetField<double>(4),
                Low = csvReader.GetField<double>(5)
            };
        }
    }
}