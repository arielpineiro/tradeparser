namespace Trader.CommandLineParser
{
    internal class Result
    {
        /// <summary>
        ///     Gets or sets the saved.
        /// </summary>
        /// <value>
        ///     The saved.
        /// </value>
        public int Saved { get; set; }

        /// <summary>
        ///     Gets or sets the ignored.
        /// </summary>
        /// <value>
        ///     The ignored.
        /// </value>
        public int Ignored { get; set; }

        /// <summary>
        ///     Gets or sets the failed.
        /// </summary>
        /// <value>
        ///     The failed.
        /// </value>
        public int Failed { get; set; }
    }
}