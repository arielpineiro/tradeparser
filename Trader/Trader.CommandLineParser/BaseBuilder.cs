﻿using CsvHelper;
using Trader.CommandLineParser.Model;

namespace Trader.CommandLineParser
{
    internal abstract class BaseBuilder
    {
        /// <summary>
        ///     The CSV reader
        /// </summary>
        protected readonly CsvReader CsvReader;

        protected BaseBuilder(CsvReader csvReader)
        {
            CsvReader = csvReader;
        }

        /// <summary>
        ///     Creates the call.
        /// </summary>
        /// <returns></returns>
        public abstract Option CreateCall();

        /// <summary>
        ///     Creates the put.
        /// </summary>
        /// <returns></returns>
        public abstract Option CreatePut();
    }
}