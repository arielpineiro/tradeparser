USE [Trader]

DECLARE @Date AS Date = '2017-07-21'
DECLARE @Symbol AS nvarchar(50) = 'AAPL'

SELECT
	symbols.[Name],
	stockAndOptionsQuotes.[Date],
	options.[Last],
	options.Mark,
	options.Bid,
	options.Ask,
	options.Strike
FROM Trader.dbo.OptionChains optionChains
JOIN dbo.Options options 
	ON options.OptionChain_Id = optionChains.Id
LEFT JOIN dbo.Calls calls 
	ON calls.Id = options.Id
LEFT JOIN dbo.Puts puts 
	ON puts.Id = options.Id
JOIN dbo.StockAndOptionQuotes stockAndOptionsQuotes 
	ON stockAndOptionsQuotes.Id = optionChains.StockAndOptionQuote_Id
JOIN dbo.Symbols symbols 
	ON symbols.Id = stockAndOptionsQuotes.Symbol_Id
WHERE optionChains.[Date] = @Date 
	AND symbols.[Name] = @Symbol