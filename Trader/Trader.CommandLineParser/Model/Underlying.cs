using System.ComponentModel.DataAnnotations;

namespace Trader.CommandLineParser.Model
{
    /// <summary>
    ///     Underlyings
    /// </summary>
    public class Underlying
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the stock and option quote.
        /// </summary>
        /// <value>
        ///     The stock and option quote.
        /// </value>
        [Required]
        public StockAndOptionQuote StockAndOptionQuote { get; set; }

        /// <summary>
        ///     Gets or sets the last.
        /// </summary>
        /// <value>
        ///     The last.
        /// </value>
        public double Last { get; set; }

        /// <summary>
        ///     Gets or sets the net CHNG.
        /// </summary>
        /// <value>
        ///     The net CHNG.
        /// </value>
        public double NetChng { get; set; }

        /// <summary>
        ///     Gets or sets the open.
        /// </summary>
        /// <value>
        ///     The open.
        /// </value>
        public double Open { get; set; }

        /// <summary>
        ///     Gets or sets the high.
        /// </summary>
        /// <value>
        ///     The high.
        /// </value>
        public double High { get; set; }

        /// <summary>
        ///     Gets or sets the low.
        /// </summary>
        /// <value>
        ///     The low.
        /// </value>
        public double Low { get; set; }
    }
}