﻿namespace Trader.CommandLineParser.Model
{
    /// <summary>
    ///     Options
    /// </summary>
    public abstract class Option
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the option chain.
        /// </summary>
        /// <value>
        ///     The option chain.
        /// </value>
        public OptionChain OptionChain { get; set; }

        /// <summary>
        ///     Gets or sets the option chain identifier.
        /// </summary>
        /// <value>
        ///     The option chain identifier.
        /// </value>
        public int OptionChain_Id { get; set; }

        /// <summary>
        ///     Gets or sets the last.
        /// </summary>
        /// <value>
        ///     The last.
        /// </value>
        public double Last { get; set; }

        /// <summary>
        ///     Gets or sets the mark.
        /// </summary>
        /// <value>
        ///     The mark.
        /// </value>
        public double Mark { get; set; }

        /// <summary>
        ///     Gets or sets the bid.
        /// </summary>
        /// <value>
        ///     The bid.
        /// </value>
        public double Bid { get; set; }

        /// <summary>
        ///     Gets or sets the ask.
        /// </summary>
        /// <value>
        ///     The ask.
        /// </value>
        public double Ask { get; set; }

        /// <summary>
        ///     Gets or sets the strike.
        /// </summary>
        /// <value>
        ///     The strike.
        /// </value>
        public double Strike { get; set; }
    }
}