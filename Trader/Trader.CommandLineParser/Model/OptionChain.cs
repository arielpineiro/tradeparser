﻿using System;
using System.Collections.Generic;

namespace Trader.CommandLineParser.Model
{
    /// <summary>
    ///     Options Chain
    /// </summary>
    public sealed class OptionChain
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the stock and option quote.
        /// </summary>
        /// <value>
        ///     The stock and option quote.
        /// </value>
        public StockAndOptionQuote StockAndOptionQuote { get; set; }

        /// <summary>
        ///     Gets or sets the stock and option quote identifier.
        /// </summary>
        /// <value>
        ///     The stock and option quote identifier.
        /// </value>
        public int StockAndOptionQuote_Id { get; set; }

        /// <summary>
        ///     Gets or sets the options.
        /// </summary>
        /// <value>
        ///     The options.
        /// </value>
        public ICollection<Option> Options { get; set; } = new HashSet<Option>();

        /// <summary>
        ///     Gets or sets the date.
        /// </summary>
        /// <value>
        ///     The date.
        /// </value>
        public DateTime Date { get; set; }
    }
}