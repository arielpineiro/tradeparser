﻿using System.Data.Entity;

namespace Trader.CommandLineParser.Model
{
    /// <summary>
    ///     Db
    /// </summary>
    /// <seealso cref="DbContext" />
    public class TraderDb : DbContext
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TraderDb" /> class.
        /// </summary>
        public TraderDb() : base("Trader")
        {
        }

        /// <summary>
        ///     Gets or sets the symbols.
        /// </summary>
        /// <value>
        ///     The symbols.
        /// </value>
        public DbSet<Symbol> Symbols { get; set; }

        /// <summary>
        ///     Gets or sets the stock and option quotes.
        /// </summary>
        /// <value>
        ///     The stock and option quotes.
        /// </value>
        public DbSet<StockAndOptionQuote> StockAndOptionQuotes { get; set; }

        /// <summary>
        ///     This method is called when the model for a derived context has been initialized, but
        ///     before the model has been locked down and used to initialize the context.  The default
        ///     implementation of this method does nothing, but it can be overridden in a derived class
        ///     such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        ///     Typically, this method is called only once when the first instance of a derived context
        ///     is created.  The model for that context is then cached and is for all further instances of
        ///     the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///     property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///     More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///     classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Call>().ToTable("Calls");
            modelBuilder.Entity<Put>().ToTable("Puts");

            //one-to-many
            modelBuilder.Entity<OptionChain>()
                .HasRequired(x => x.StockAndOptionQuote)
                .WithMany(x => x.OptionsChains)
                .HasForeignKey(x => x.StockAndOptionQuote_Id);

            //one-to-many
            modelBuilder.Entity<Option>()
                .HasRequired(x => x.OptionChain)
                .WithMany(x => x.Options)
                .HasForeignKey(x => x.OptionChain_Id);
        }
    }
}