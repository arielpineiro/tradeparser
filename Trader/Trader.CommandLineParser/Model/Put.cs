using System.ComponentModel.DataAnnotations.Schema;

namespace Trader.CommandLineParser.Model
{
    /// <summary>
    ///     Puts
    /// </summary>
    /// <seealso>
    ///     <cref>Trader.CommandLineParser.Model.Option</cref>
    /// </seealso>
    [Table("Puts")]
    public class Put : Option
    {
    }
}