﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Trader.CommandLineParser.Model
{
    /// <summary>
    ///     Symbols
    /// </summary>
    public class Symbol
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        [Index(IsUnique = true)]
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
    }
}