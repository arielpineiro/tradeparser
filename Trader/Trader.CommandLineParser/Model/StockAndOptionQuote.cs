﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Trader.CommandLineParser.Model
{
    /// <summary>
    ///     Stock
    /// </summary>
    public class StockAndOptionQuote
    {
        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the symbol.
        /// </summary>
        /// <value>
        ///     The symbol.
        /// </value>
        [Required]
        public virtual Symbol Symbol { get; set; }

        /// <summary>
        ///     Gets or sets the date.
        /// </summary>
        /// <value>
        ///     The date.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        ///     Gets or sets the think back.
        /// </summary>
        /// <value>
        ///     The think back.
        /// </value>
        [StringLength(255)]
        public string ThinkBack { get; set; }

        /// <summary>
        ///     Gets or sets the options chains.
        /// </summary>
        /// <value>
        ///     The options chains.
        /// </value>
        public virtual ICollection<OptionChain> OptionsChains { get; set; } = new HashSet<OptionChain>();

        /// <summary>
        ///     Gets or sets the underlying.
        /// </summary>
        /// <value>
        ///     The underlying.
        /// </value>
        public virtual Underlying Underlying { get; set; }
    }
}