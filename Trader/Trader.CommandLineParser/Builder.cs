﻿using CsvHelper;
using Trader.CommandLineParser.Model;

namespace Trader.CommandLineParser
{
    internal class Builder : BaseBuilder
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Builder" /> class.
        /// </summary>
        /// <param name="csvReader">The CSV reader.</param>
        public Builder(CsvReader csvReader) : base(csvReader)
        {
        }

        /// <summary>
        ///     Creates the call.
        /// </summary>
        /// <returns></returns>
        public override Option CreateCall()
        {
            return new Call
            {
                Last = CsvReader.GetField<double>(2),
                Mark = CsvReader.GetField<double>(3),
                Bid = CsvReader.GetField<double>(4),
                Ask = CsvReader.GetField<double>(5),
                Strike = CsvReader.GetField<double>(7)
            };
        }

        /// <summary>
        ///     Creates the put.
        /// </summary>
        /// <returns></returns>
        public override Option CreatePut()
        {
            return new Put
            {
                Bid = CsvReader.GetField<double>(8),
                Ask = CsvReader.GetField<double>(9),
                Last = CsvReader.GetField<double>(10),
                Mark = CsvReader.GetField<double>(11),
                Strike = CsvReader.GetField<double>(7)
            };
        }
    }
}