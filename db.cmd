@echo off
cmd /c "build.cmd"
set Migrate=Trader\packages\EntityFramework.6.4.0\tools\net45\any\ef6.exe 
%Migrate% database update --assembly Trader\Trader.CommandLineParser\bin\Release\Trader.CommandLineParser.exe --config "%~dp0\Trader\Trader.CommandLineParser\bin\Release\Trader.CommandLineParser.exe.config" --project-dir "Trader\Trader.CommandLineParser\bin\Release" -v
echo.
sqlcmd -S .\SQLEXPRESS -d Trader -Q "SELECT * FROM Symbols ORDER BY Id"